; Include SNES Initialization code
.include "header.inc"
.include "snes_init.asm"

; VBlank interruption (see header.inc)
VBlank:
  RTI

.bank 0
.section "MainCode"

Start:
  ; Initialize the SNES
  Snes_Init

  ; Set the color to pink
  sep #$20       ; Set the A register to 8 bit
  lda #%10000000 ; Force VBlank
  sta $2100

  lda #%00011111 ; low byte: gggrrrrr
  sta $2122
  lda #%01111100 ; high byte: -bbbbbgg
  sta $2122

  lda #%00001111 ; End VBlank, set brightness to 15
  sta $2100

; Start Game Loop
GameLoop:
  jmp GameLoop

.ends
