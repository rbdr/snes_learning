;==LoRom==                                ; This is a Slow Rom / LoRom config

.MEMORYMAP                                ; Describe the system architecture
  SLOTSIZE      $8000                     ; Set the slot size
  DEFAULTSLOT   0
  SLOT          0       $8000             ; Slot 0's starting address
.ENDME

.ROMBANKSIZE    $8000                     ; Sets ROM bank to 32KBytes
.ROMBANKS       8                         ; Use 8 Rom Banks (2Mbits)

.SNESHEADER
  ID            "SNES"                    ; 1 to 4 chars
  NAME          "SNES Demo            "   ; Exactly 21 chars.
; Name Guide    "123456789012345678901"

  SLOWROM
  LOROM

  ; See http://www.villehelin.com/wla.txt
  CARTRIDGETYPE $00                       ; ROM Only cartridge
  ROMSIZE       $08                       ; 2 Mbit rom size
  SRAMSIZE      $00                       ; No SRAM
  COUNTRY       $01                       ; $01 US; $00 JP; $02 AU/NZ/EU/ASIA;
                                          ; $03 SE; $04 FI; $05 DK; $06 FR;
                                          ; $07 NL; $08 ES; $09 DE/AT/CH;
                                          ; $0A IT; $0B HK/CN; $0C ID; 0D KR;
  LICENSEECODE  $00
  VERSION       $00                       ; 00 = 1.00, $01 = 1.01 (1.byte)
.ENDSNES

.SNESNATIVEVECTOR                         ; Native Mode Interrupt Vector Table
  COP           EmptyHandler              ; COP = Coprocessor Interrupt (Software Interrupt)
  BRK           EmptyHandler              ; BRK = Software Interrupt
  ABORT         EmptyHandler              ; ABORT = Hardware Exception Interrupt
  NMI           VBlank                    ; NMI = Non Maskable Interrupt (Can't Recover)
  IRQ           EmptyHandler              ; IRQ = Interrupt Request (Maskable Interrupt)
.ENDNATIVEVECTOR

.SNESEMUVECTOR                            ; Emulation Mode Interrupt Vector Table
  COP           EmptyHandler
  ABORT         EmptyHandler
  NMI           EmptyHandler
  RESET         Start                     ; RESET = Reset Signal
  IRQBRK        EmptyHandler
.ENDEMUVECTOR

.BANK           0 SLOT 0                  ; Use ROM Bank 0 and Memory Slot 0
.ORG            0                         ; Starting Address relative to the Bank / Slot

.SECTION        "EmptyVectors" SEMIFREE   ; Start a section
                                          ; FORCE = place at .ORG
                                          ; FREE = place in same bank where there is room (Default)
                                          ; SEMIFREE = place where you can starting from .ORG
                                          ; SUPERFREE = place in a bank where you can

EmptyHandler:
  RTI                                     ; RTI = Return from Interrupt

.ENDS                                     ; End section

.EMPTYFILL $00                            ; Fills unused areas with $00 AKA BRK
                                          ; Will crash if use is attempted
