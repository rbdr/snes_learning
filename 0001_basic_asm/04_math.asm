; Increase and Decrease

INC $0DBF   ; Adds one to the address $0DBF
RTS

DEC $0DBF   ; Decreases one from the address $0DBF
RTS

; INC and DEC only work with direct or absolute address ($XX or $XXXX)

; Addition

LDA $0DBF   ; Loads the number of coins to the accumulator
CLC         ; We must set carry to 0 before doing an add
ADC #$20    ; ADC = Add With Carry. CLC = Clear Carry
STA $0DBF   ; Stores the accumulator to the coins
RTS

; Substraction

LDA $0DBF   ; Loads the number of coins to the accumulator
SEC         ; Sets carry
SBC #$20    ; SBC = Substract with Carry. SEC = Set Carry
STA $0DBF   ; Stores the accumulator to hte coins
RTS

; Multiplication
LDA $0DBF   ; Loads the number of coins to the accumulator
ASL A       ; Shifts the accumulator to the left (multiplies x2)
STA $0DBF   ; Stores the contents of the acumulator
RTS

; LSR       ; Logic Shift Right. Shifts to the right (/2)
