; Reminder, mario's state is in $7E0019
; 00 = small mario
; 01 = big mario
; 02 = caped mario
; 03 = fiery mario

LDA $19	        ; Sets the accumulator to the current mario state
CMP #$00        ; Compare the accumulator with 00
BEQ MakeFiery   ; Bumps to MakeFiery block if equal

; Code branches:
; BEQ - Branch if Equal
; BNE - Branch if Not Equal
; BCC - Branch if Less
; BCS - Branch if Greater
; BRA - Branch Always

RTS

MakeFiery:
  LDA #$03  ; Stores Fiery mario to the accumulator
  STA $19   ; Sets the current mario state to fiery
  RTS
