; $1490 stores the timer for the star power up.
; Mario's state is in $7E0019:
; 00 = small mario
; 01 = big mario
; 02 = caped mario
; 03 = fiery mario

; The following program sets mario's star timer based on
; his current status.
  LDA $19
  CMP #$00
  BEQ SetSmallMarioTimer
  CMP #$01
  BEQ SetBigMarioTimer
  CMP #$02
  BEQ SetCapedMarioTimer
  CMP #$03
  BEQ SetFieryMarioTimer
  RTL

SetSmallMarioTimer:
  LDA #$1C          ; Sets to 28 seconds
  BRA SetTimer

SetBigMarioTimer:
  LDA #$28          ; Sets to 40 seconds
  BRA SetTimer

SetCapedMarioTimer:
  LDA #$38          ; Sets to 56 seconds
  BRA SetTimer

SetFieryMarioTimer:
  LDA #$56          ; Sets to 86 seconds

SetTimer:
  STA $1490         ; Stores the mario timer
  RTL
