; X and Y are also registers, so if accumulator is:
; X and Y are usually tables for lookup

; LDA / LDX / LDY
; STA / STX / STY
; INC / INX / INY
; DEC / DEX / DEY
; CMP / CPX / CPY

LDX #20 ; Loads 20 to the x register
LDY #32 ; Loads 32 to the y register

LDX #$20 ; Loads 32 to the x register
LDY #$32 ; Loads 50 to the y register

STX $19 ; Store x register into mario's state
STY $19 ; Store y register into mario's state

RTL

; INX / INY / DEX / DEY cannot act on memory addresses
; You must use LDX + INX + STX / LDY + INY + STY instead

Table:
db $3F,$1A,$2C,$01,$08,$24
;   00  01  02  03  04  05
