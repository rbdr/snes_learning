; JSR - Jump to Subroutine
; JSL - Jump to Subroutine Long
; JMP - Jump (Same as BRA - Break Always)
; JML - Jump Long

; JSR — Jumping to symbols or parts of the ROM to execute
LDA #$02                ; Loads two to the accumulator
JSR StoreMarioState     ; Jumps to StoreMarioState subroutine
RTS

StoreMarioState:
  STA $19               ; Stores the current value of the accumulator to $19
  RTS

; REF - Mario Rom Map @ http://www.smwcentral.net/?p=map&type=rom
; $0294C1 contains "Cape Mario smashes ground subroutine."
; (Bank $02, Address $94C1)

JSR $0294C1             ; Jumps to "Cape Mario smashes ground subroutine."
RTS

; For long jumps we use JSL + RTL instead of JSR + RTS
; Short jumps -> 128 bytes
; Long jump -> 8000 bytes

; JML - Jumping to ROM, ignores the rest of the code

JML $00F606             ; Jump to kill mario
STZ $0DBF               ; Ignored
RTL                     ; Ignored

; JMP only allows you to jump to ROM in the current bank
