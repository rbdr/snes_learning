LDA #20	; Loads 20 to the accumulator
LDA #32	; Loads 32 to the accumulator

LDA #$20	; Loads 32 to the accumulator
LDA #$32	; Loads 50 to the accumulator

; Address $7E0019 controls mario's state.
; 00 = small mario
; 01 = big mario
; 02 = caped mario
; 03 = fiery mario
; Can be shortened to $19 as 7E is implied(?)
; and trailing zeroes are not required.

; Make mario big
LDA #$01	; load one into the accumulator
STA $19	; store the contents of the accumulator to $7E0019

RTS	; Return
