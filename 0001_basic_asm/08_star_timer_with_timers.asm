; vim: ft=65816
; $1490 stores the timer for the star power up.
; Mario's state is in $7E0019:
; 00 = small mario
; 01 = big mario
; 02 = caped mario
; 03 = fiery mario

; The following program sets mario's star timer based on
; his current status.
Timers:
db $08,$1C,$24,$48

LDX $19               ; Load the status of mario to x to use to index the table
LDA Timers,x          ; Load the timer to the accumulator by index
STA $1490
RTL

; Playing with registers
; TAX, TAY, TXY, TXA, TYX, TYA
; Copies the address one register to another
; eg. TAX -> A to X, TXY -> X to Y, etc
