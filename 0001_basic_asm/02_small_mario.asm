LDA #20	; Loads 20 to the accumulator
LDA #32	; Loads 32 to the accumulator

LDA #$20	; Loads 32 to the accumulator
LDA #$32	; Loads 50 to the accumulator

; Address $7E0019 controls mario's state.
; 00 = small mario
; 01 = big mario
; 02 = caped mario
; 03 = fiery mario
; Can be shortened to $19 as 7E is implied(?)
; and trailing zeroes are not required.

; Make mario big
STZ $19	; Sets $7E0019 to zero

; Set To Zero does not work in long address modes

; Guide to locations
; $0000-$1FFF = RAM
; $2000-$7FFF = Hardware Registers
; $8000-$FFFF = ROM
; Long addressing allows us to read from other banks

RTS	; Return
